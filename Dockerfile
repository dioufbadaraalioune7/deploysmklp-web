# Utiliser l'image Nginx alpine
FROM nginx:alpine

# Copier les fichiers buildés dans le répertoire d'assets de Nginx
COPY . /usr/share/nginx/html

# Exposer le port 80
EXPOSE 80

# Démarrer Nginx
CMD ["nginx", "-g", "daemon off;"]
